import { Plugin } from 'ckeditor5/src/core';
import { ContextualBalloon } from 'ckeditor5/src/ui';

export default class Logging extends Plugin {

	init() {
    console.log( 'LoggingUI#init() got called' );

		const editor = this.editor;
		const model = editor.model;
		const balloon = this.editor.plugins.get( ContextualBalloon );

		editor.on('ready', () => {
			console.log('%cEditor is ready - listened from within Logging plugin via listener', 'color: purple; font-weight: bold')
		})

		model.document.on('change:data', (evt, args) => {
			this._logMessage('|  The document data structure has changed (selections are excluded)');
			const changes = {
				['Last change type']: model.document.history.lastOperation.type,
				['Is selection collapsed']: model.document.selection.isCollapsed,
			}
			console.table(changes);
		})

		balloon.on('change:visibleView', (eventInfo, name, value, oldValue) =>  {
			this._logMessage('|  The ballon visible view has changed:');
			const changes = {
				['New view']: value ? value.constructor.name : '- None -',
				['Old view']: oldValue ? oldValue.constructor.name : '- None -',
			};
			console.table(changes);
		})

		// Log the pipeline.
		editor.data.on('init', () => this._logMessage('[DataController] - Init'));
		editor.data.on('ready', () => this._logMessage('[DataController] - Ready'));
		editor.data.on('set', () => this._logMessage('[DataController] - Set'));
		editor.data.on('get', () => this._logMessage('[DataController] - Get'));

		editor.editing.view.on('render', () => this._logMessage('[View] - Rendered'));
		editor.editing.view.on('change:document', () => this._logMessage('[View] - Document property changed'));
		editor.editing.view.on('change:hasDomSelection', () => this._logMessage('[View] - hasDomSelection property changed'));
	}

	_logMessage(message) {
		console.log('%c%s', 'color: purple;font-weight: bold', message);
	}
}
