import { BoldOverride as BoldOverrideDll, icons } from '../src';
import BoldOverride from '../src/boldoverride';

import ckeditor from './../theme/icons/ckeditor.svg';

describe( 'CKEditor5 BoldOverride DLL', () => {
	it( 'exports BoldOverride', () => {
		expect( BoldOverrideDll ).to.equal( BoldOverride );
	} );

	describe( 'icons', () => {
		it( 'exports the "ckeditor" icon', () => {
			expect( icons.ckeditor ).to.equal( ckeditor );
		} );
	} );
} );
